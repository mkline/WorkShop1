DROP DATABASE IF EXISTS workshop1;
CREATE DATABASE workshop1;
\c workshop1
CREATE ROLE hostman  with login;
CREATE TABLE workshops(
	ID serial NOT NULL,
	Course varchar(35) NOT NULL default '',
	Attendee varchar(35) NOT NULL default '',
	PRIMARY KEY (ID)
);
alter user hostman with password 'lD3V3cj1RU';
grant select on workshops to hostman;
grant usage, select on SEQUENCE workshops_id_seq to hostman;
INSERT INTO workshops (Course, Attendee) VALUES 
	('DevOps 101', 'Matt Kline'),
	('DevOps 101', 'Karla Cale'),
	('DevOps 101', 'Max Snark'),
	('Machine Learning', 'Joe Clark'),
	('Machine Learning', 'Jane Doe'),
	('MongoDB', 'John Smith'),
    ('React Fundamentals','Bobby Bueche'),
    ('Self-Driving Cars', 'Bill Madison');


	
