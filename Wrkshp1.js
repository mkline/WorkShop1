const express = require('express');
var Pool = require('pg').Pool;
var bodyParser = require('body-parser');

const app = express();

var config = {
  host: 'localhost',
  user: 'hostman',
  password: 'lD3V3cj1RU',
  database: 'workshop1',
};

var pool = new Pool(config);

app.set('port',(8080));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));
app.get('/api', async (req, res) => {
	var x = req.query.workshop;
	var work = await pool.query('select 1 from workshops where Course = $1',[x]);
	if(!x){
		try{
			var response = await pool.query('select distinct Course from workshops');
			console.log(JSON.stringify(response.rows));
			var c = response.rows.map(function(item){
				return item.course;
			})
			res.json({'workshops': c});
		}
		catch(e){
			console.error('Error running query ' + e);
		}
	}
	else if(work.rows.length > 0){
		try{
                        var response = await pool.query('select Attendee from workshops where Course = $1',[x]);
                        console.log(JSON.stringify(response.rows));
			var peep = response.rows.map(function(item){
				console.log(item.attendee);
				return item.attendee;
			})
                        res.json({'attendees': peep});
			console.log(work.rows.length);
                }
                catch(e){
                        console.error('Error running query ' + e);
                }
	}
	else{
		res.json({error: 'workshop not found'});
	}
});

app.post('/api', async (req, res) => {
	console.log(req.body);
	var Course = req.body.workshop;
	var Attendee = req.body.attendee;
	if(!Course || !Attendee){
		res.json({error: 'parameters not given'});
	}
	else{
		var tester = await pool.query('select 1 from workshops where Attendee = $2 and Course = $1', [Course, Attendee]);
		if(tester.rows.length < 1){ 
			try{
				var response = await pool.query('insert into workshops (Course, Attendee) values ($1, $2)', [Course, Attendee]);
				res.json({'attendee': Attendee, 'workshop': Course});
			}
			catch(e){
				console.log('error running insert', e);
			}
		}
		else{
			res.json({error: 'attendee already enrolled'});
		}
	}
});
app.listen(app.get('port'), () =>{
	console.log('Running');
});

